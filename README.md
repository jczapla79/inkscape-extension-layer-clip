```


.__                                            .__   .__         
|  |  _____    ___.__.  ____  _______    ____  |  |  |__|______  
|  |  \__  \  <   |  |_/ __ \ \_  __ \ _/ ___\ |  |  |  |\____ \ 
|  |__ / __ \_ \___  |\  ___/  |  | \/ \  \___ |  |__|  ||  |_> >
|____/(____  / / ____| \___  > |__|     \___  >|____/|__||   __/ 
           \/  \/          \/               \/           |__|    


http://www.pernsteiner.org/inkscape/layerclip/
Copyright (c) 2012 Stuart Pernsteiner
```
# Disclaimer

i am not the author of this great inkscape extension. All credits & copyrights goto Stuart Pernsteiner.

# Reason for Gitlab Repository

The author has no github / gitlab page. with the release of inkscape 1.0 the extension became not usable since it used some deprecated method calls. since i am using it all the time to create rad vector graffiti graphics, i wanted it to become working again.
since i am a developer myself and i believe in the power of open source i decided to fix the errors and create a repository for the code. all original copyright disclaimers are included.


Inkscape Notes on updating extensions:
https://wiki.inkscape.org/wiki/index.php/Updating_your_Extension_for_1.0

# Installation

## Windows

copy all files from this repository to: C:\Users\<Your Username>\AppData\Roaming\inkscape\extensions

## Linux

copy all files from this repository to: ~/.config/inkscape/extensions

# Original Readme / NFO

Layer Clip Extension

This is a a set of Inkscape extension scripts for setting and removing clip paths for entire layers. Inkscape has no trouble rendering or editing layers with clip paths, but it currently has no built-in method of setting the clip path for a layer&emdash;that is the purpose of these extensions.

Download:
inkscape-layerclip-0.1.1.zip

Installation:
Simply unzip the downloaded file into the following directory:

    Windows: C:\Program Files\Inkscape\share\extensions
    Linux: /usr/share/inkscape/extensions (system-wide install) or ~/.config/inkscape/extensions (single-user install)
    OS X: /Applications/Inkscape.app/Contents/Resources/extensions

Usage:
(Or, see an example with screenshots)
Once the extensions are installed, a "Layer Clip" submenu will appear within Inkscape's "Extensions" menu. The "Layer Clip" menu provides several options:

    Clip containing layer: Select a single object and then run this extension to set that object as the clip path for the layer that contains it. This will mark the layer with "(c*)" to indicate that the layer contains an object that is being used to clip its containing layer, and with "(C)" to indicate that the layer has a clip path applied to it. (These markers are simply added to the layer name to make it easier to tell when clip paths are in use.)
    Clip layer above: Like "Clip containing layer", but sets the object as the clip path for the layer above the one that contains it. The layer containing the object will be marked with "(c+)" (instead of "(c*)" for "Clip containing layer").
    Clip layer below: Like "Clip containing layer", but on the layer below the layer containing the selected object. Marks the object's layer with "(c-)".
    Clip parent layer: Like "Clip containing layer", but on the parent layer of the layer containing the selected object. Marks the object's layer with "(c**)".
    Remove layer clip: Select an object in a clipped layer and then run this extension to remove the clip path from a layer. The "(C)" marker will also be removed, if one is present. The marker on the layer that held the actual clip path will not be removed, but you can remove it yourself by manually editing the layer name.
    Reset clippath transforms: If you move an object that is being used as a clip path for a layer, the layer will still be clipped based on the old position. Run this extension to fix the problem.

from authors page:
http://www.pernsteiner.org/inkscape/layerclip/


